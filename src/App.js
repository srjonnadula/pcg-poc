
import './App.css';
import { config } from './config';
import { useMsal } from "@azure/msal-react";
import { PublicClientApplication } from '@azure/msal-browser'
import { Component } from 'react';
import UserList from './Components/User_Managment/userList';


class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isAuthenticated: false,
      user: {}
    };
    this.login = this.login.bind(this);
    this.publicClientApplication = new PublicClientApplication({
      auth: {
        clientId: config.appid,
        redirectUri: config.redirectUri,
        authority: config.authority
      },
      cache: {
        cacheLocation: 'cookie',
        storeAuthStateInCookie: true
      }
    });
  }

  async login() {
    try {
    //   const { instance, accounts } = useMsal();
    //   instance.acquireTokenSilent({
    //     ...loginRequest,
    //     account: accounts[0]
    // }).then((response) => {
    //     //callMsGraph(response.accessToken).then(response => setGraphData(response));
    //     this.setState({ isAuthenticated: true, token: response.accessToken });
    // });
    
     const response =  await this.publicClientApplication.loginPopup({
        scopes: config.scopes,
        prompt: "select_account"
      })
      this.setState({ isAuthenticated: true, token: response.accessToken });
    //   //alert(xyz.accessToken)
       console.log(response.accessToken);
    }
    catch (err) {
      this.setState({
        isAuthenticated: false,
        user: {},
        error: err
      })
    }
  }

  logout() {
    this.publicClientApplication.logout();
  }
  
  render() {
    return (
      <div className="App">
        <header className='App-header'>
          {this.state.isAuthenticated ?
            <UserList token={this.state.token}/>
            :
            <p>
              <button onClick={() => this.login()}>Log In</button>
            </p>
          }
        </header>
      </div>
    )
  }
}
export default App;

// function App() {
//   const [lilist, setlilist] = useState([]);
//   const makeAPICall = async () => {
//     try {
//       const response = await fetch('http://localhost:8080/cors', { mode: 'cors' });
//       const data = await response.json();
//       console.log({ data })
//     }
//     catch (e) {
//       console.log(e)
//     }
//   }
//   const makeservercall = async () => {
//     fetch('https://navi-lp7-ms-apimgmt-svc.azure-api.net/fss-dev/api/Metadata',
//       {
//         method: 'get',
//         headers: {
//           "mode": 'cors',
//           'Access-Control-Allow-Origin': "*",
//           //"Access-Control-Allow-Origin": "*",
//           "X-Application-Id": "test",
//           "Ocp-Apim-Subscription-Key": "e205d9f2a4bb49a7ad6c06b0dc2decb3"
//         }
//       })
//       .then(res => res.json())
//       .then(data => {
//         alert(data);
//         setlilist(data);
//       })
//   }
//   useEffect(() => {
//     makeAPICall();
//   }, [])


//   return (
//     <div className="App">
//       <h1>PCG </h1>
//       {/* {!!lilist.length &
//         lilist.map((d) => {
//           return <p>{d.id}</p>
//         })
//       } */}
//     </div>
//   );
// }

// export default App;
