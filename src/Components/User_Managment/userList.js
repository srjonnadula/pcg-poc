import React, { useEffect, useState } from "react";
import axios from "axios";

export default function UserList(props) {
    
    const [usersList, setUsersList] = useState([]);
    const fetchData = async () => {
        const b = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjJaUXBKM1VwYmpBWVhZR2FYRUpsOGxWMFRPSSJ9.eyJhdWQiOiI1NWFiNTViZS1mMjRiLTRjMTAtYTNkNy0xNGU2YmQ2ZWU0NDkiLCJpc3MiOiJodHRwczovL2xvZ2luLm1pY3Jvc29mdG9ubGluZS5jb20vN2VmOTgzYjEtMWU3NC00Yzg5LTgzMjktZTQ0NmY5OTg3NmFkL3YyLjAiLCJpYXQiOjE2NTg0MTk2OTQsIm5iZiI6MTY1ODQxOTY5NCwiZXhwIjoxNjU4NDI1MDk4LCJhaW8iOiJBV1FBbS84VEFBQUFiMXVIbm1pSDZ5eDBkL3dySzd2VjUzT1pxN21lMUxpNWFzREdxT1ZWaFkxbFlwRWdRZWloSHBJdzRRRG44YUNOTGt3Yy85V1QrMGg1SmFvUHRjMEFORzF1bzhsbWZJbmRBWUJGZVh1YllJTWZsUkNqTHFsd1E0aExaSmVZTUdFUiIsImF6cCI6ImY0MWM0NDQ0LWM3NDktNDUxYy05ZWQ1LWU3OWJiY2UyMWRiNCIsImF6cGFjciI6IjEiLCJpZHAiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC84NDZiNzRiMy0zZGQxLTRjODEtYTZmOS0yYmQ0NDFlN2ZhMTgvIiwibmFtZSI6IlNydWphbiBKb25uYWR1bGEiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJzcmpvbm5hZHVsYUBjc3BuYXZpc2l0ZS5vbm1pY3Jvc29mdC5jb20iLCJyaCI6IjAuQVc0QXNZUDVmblFlaVV5REtlUkctWmgycmI1VnExVkw4aEJNbzljVTVyMXU1RWx1QU9jLiIsInNjcCI6IkZpbGUuUmVhZCIsInN1YiI6IlB3UFdsU3VCeC0weXNCbjVtYmd5YlMtYkRjSnNqX3YzWHBMLTFXZ0FsNTAiLCJ0aWQiOiI3ZWY5ODNiMS0xZTc0LTRjODktODMyOS1lNDQ2Zjk5ODc2YWQiLCJ1dGkiOiJsX1J6M2hYeE5rR0s0cmVLY3MzMkFBIiwidmVyIjoiMi4wIn0.t_Uh-TxmvtwcjaRE8FmwBqUkKgwBdF3floHp4nM95ydP7Aa9vmplDn8tfbvw-_UOgTfCLCpRA0ey0NpdMnjTP87bUho4jRZX8VrL7fyrnjfDUVy2WEOyAFyYoS06UmNCN20cE21ptjz8-XnxlzKcGuRnneRY7qyxE03HB67QX0EMpMTko0f6WGP0bDwDOR0ptu7XTsToqPczaRDgfdzx-gVRvVFSD1T8ANxUsXF1LfccfV_v0lkXJjeiKv5Zz9LdvWu9xfyXTGPjsXl5rC1SoNqdhBXI1k6MmXL6uGEGxBTv6Ujb8a1sAu8E8f8tUh_wIEkdLJ4xJ_uxWzQaXBUU7A';
        const url = 'https://navi-lp7-ms-apimgmt-svc.azure-api.net/fss-dev-clone/api/Metadata'

        const result = await fetch(url, {
            method: 'get',
            headers: new Headers( {
                'X-Application-Id':'test',
                'Access-Control-Allow-Origin': '*',
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": 'Bearer '+ props.token
            })
          });
    }

    useEffect(() => {
        fetchData()
            .catch(console.error);
    }, [fetchData]);

    return (
        <div style={{ width: 900}}>
        <h1>Logged In</h1>

        <div>
            <h1>Access token :</h1>
            <div style={{ width: 900, 'overflow-wrap': 'break-word'}}>{props.token}</div>
        </div>
        </div>
    )
}